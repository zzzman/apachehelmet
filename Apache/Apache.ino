#include <Servo.h>
#include <SoftwareSerial.h>

Servo myservo;
int d = 90;

 
#define DEBUG false
 
SoftwareSerial esp8266(2,3); // make RX Arduino line is pin 2, make TX Arduino line is pin 3.
                             // This means that you need to connect the TX line from the esp to the Arduino's pin 2
                             // and the RX line from the esp to the Arduino's pin 3
void setup()
{
  Serial.begin(115200);
  esp8266.begin(115200); // your esp's baud rate might be different
  sendData(F("AT+RST"),2000,DEBUG); // reset module
  sendData(F("AT+CWMODE=2"),1000,DEBUG); // configure as access point
  sendData(F("AT+CIFSR"),1000,DEBUG); // get ip address
  sendData(F("AT+CIPMUX=1"),1000,DEBUG); // configure for multiple connections
  sendData(F("AT+CIPSERVER=1,80"),1000,DEBUG); // turn on server on port 80
  
  myservo.attach(9);
  myservo.write(90);
}
 
void loop()
{
  Serial.begin(115200);
  myservo.attach(9); 
  
  if(esp8266.available()) // check if the esp is sending a message 
  {
    String cmd = "";
    long rotateTime = 0;
    if(esp8266.find("+IPD,")) {
       myservo.write(90);
       delay(200);
       int connectionId = esp8266.read()-48;
       while(esp8266.available()) { 
          // The esp has data so display its output to the serial window 
          char c = esp8266.read(); // read the next character.
          cmd.concat(c);
      }  
     //Serial.println(cmd);
     int StartDegreeIndex = cmd.indexOf('d=');
     int EndDegreeIndex = cmd.indexOf('&');
     int EndRotateIndex = cmd.indexOf(' HTTP');
     Serial.println(cmd);
     int degree = cmd.substring(StartDegreeIndex+1, EndDegreeIndex).toInt();
     int rotate = cmd.substring(EndRotateIndex-5, EndRotateIndex-4).toInt();
     cmd = "";
     rotateTime = (degree) * 4.5 + 200; 
     if(DEBUG){
       Serial.print(F("Degree :"));
       Serial.println(degree);
       Serial.print(F("Rotate :"));
       Serial.println(rotate);
       Serial.println(rotateTime);
       Serial.println(rotate);
     }
     long time = millis(); 
     while( (time+rotateTime) > millis()) {  
        if(rotate == 0) {
            myservo.write(180);
        } else {
            myservo.write(0);
        }
     }
     myservo.write(90);
     
     sendData(F("AT+CIPSERVER=1,80"),1000,DEBUG);
     //String closeCommand = F("AT+CIPCLOSE="); 
    // closeCommand+=connectionId; // append connection id   
    // sendData(closeCommand,3000,DEBUG);
    }
    
    
   
  }
}
 
 
void sendData(String command, const int timeout, boolean debug)
{
    esp8266.println(command); // send the read character to the esp8266
    
    long int time = millis();
    
    while( (time+timeout) > millis())
    {
      while(esp8266.available())
      { 
        // The esp has data so display its output to the serial window 
        char c = esp8266.read(); // read the next character.
        if(debug)
        {
          Serial.print(c);
        }
      }  
    }
}
  
 

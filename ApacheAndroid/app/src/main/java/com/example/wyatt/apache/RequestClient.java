package com.example.wyatt.apache;
import android.util.Log;

import com.loopj.android.http.*;

/**
 * Created by wyatt on 2015/4/16.
 */
public class RequestClient {
    private static final String BASE_URL = "http://192.168.4.1";

    private static AsyncHttpClient client = new AsyncHttpClient();

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        Log.d("ZZZ","XXX " + url);
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }

}
